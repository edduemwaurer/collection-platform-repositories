package com.jnr.dread.repositories

import androidx.lifecycle.ViewModel
import com.jnr.dread.collection_platform_repo.repositories.AuthRepository

class MainViewModel(private val authRepository: AuthRepository) : ViewModel() {

    suspend fun login(userName: String, password: String) {
        authRepository.login(user = userName, secret = password)
    }
}