package com.jnr.dread.collection_platform_repo.repositories

import androidx.lifecycle.LiveData
import com.jnr.dread.commons.domain.DomainResources

interface ResourcesRepository {
    val allResources: LiveData<List<DomainResources>>
    suspend fun refreshAppResources(operatorToken: String)
}