package com.jnr.dread.collection_platform_repo.repositoriesImpl

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jnr.dread.collection_platform_repo.repositories.PaymentRepository

class PaymentRepositoryImpl : PaymentRepository {

    private val _payments = MutableLiveData<List<Any>>()

    override val payments: LiveData<List<Any>>
        get() = _payments

    override suspend fun refreshPayments(propertyId: Int) {
        TODO("Not yet implemented")
    }

    override suspend fun refreshPaymentsBySpan(startDate: String, endDate: String) {
        TODO("Not yet implemented")
    }

    override suspend fun refreshPaymentsByProperty(propertyId: Int) {
        TODO("Not yet implemented")
    }

    override suspend fun refreshPaymentsByPropertyUnit(propertyUnitId: Int) {
        TODO("Not yet implemented")
    }

    override suspend fun refreshPaymentsByTenant(tenantId: Int) {
        TODO("Not yet implemented")
    }
}