package com.jnr.dread.collection_platform_repo.repositoriesImpl

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jnr.dread.collection_platform_repo.repositories.ResourcesRepository
import com.jnr.dread.commons.domain.DomainResources

class ResourcesRepositoryImpl : ResourcesRepository {

    private val _allResources = MutableLiveData<List<DomainResources>>()

    override val allResources: LiveData<List<DomainResources>>
        get() = _allResources

    override suspend fun refreshAppResources(operatorToken: String) {
        TODO("Not yet implemented")
    }
}