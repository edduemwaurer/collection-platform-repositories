package com.jnr.dread.collection_platform_repo.repositoriesImpl

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jnr.dread.collection_platform_repo.repositories.DashboardRepository
import com.jnr.dread.commons.domain.DomainAppFeatures

class DashboardRepositoryImpl : DashboardRepository {

    private val _appFeatures = MutableLiveData<List<DomainAppFeatures>>()

    override val appFeatures: LiveData<List<DomainAppFeatures>>
        get() = _appFeatures

    override suspend fun refreshAllAppFeatures() {
        _appFeatures.postValue(
            listOf(
                DomainAppFeatures(1, "Property Management", "", "admiral//About"),
                DomainAppFeatures(1, "Tenancy", "", "admiral//About"),
                DomainAppFeatures(1, "About Us", "", "admiral//About"),
                DomainAppFeatures(1, "About Us", "", "admiral//About")
            )
        )
    }

    override suspend fun refreshTenantAppFeatures() {
        TODO("Not yet implemented")
    }

    override suspend fun refreshPropertyManagerAppFeatures() {
        TODO("Not yet implemented")
    }
}