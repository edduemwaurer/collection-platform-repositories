package com.jnr.dread.collection_platform_repo.repositoriesImpl

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jnr.dread.collection_platform_repo.repositories.ResourceActionsRepository
import com.jnr.dread.commons.domain.DomainResourceActions

class ResourceActionsRepositoryImpl : ResourceActionsRepository {

    private val _allResourceActions = MutableLiveData<List<DomainResourceActions>>()

    override val allResourceActions: LiveData<List<DomainResourceActions>>
        get() = _allResourceActions

    override suspend fun refreshAppResourceActions(resourceId: Long, operatorToken: String) {
        TODO("Not yet implemented")
    }
}