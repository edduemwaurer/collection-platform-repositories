package com.jnr.dread.collection_platform_repo.repositories

import androidx.lifecycle.LiveData
import com.jnr.dread.commons.domain.DomainOperator

interface AuthRepository {
    suspend fun login(user: String, secret: String): LiveData<DomainOperator>
    suspend fun register()
    suspend fun resetPassword(operatorId: Int)
}