package com.jnr.dread.collection_platform_repo.repositories

import androidx.lifecycle.LiveData

interface PaymentRepository {
    val payments: LiveData<List<Any>>
    suspend fun refreshPayments(propertyId: Int)
    suspend fun refreshPaymentsBySpan(startDate: String, endDate: String)
    suspend fun refreshPaymentsByProperty(propertyId: Int)
    suspend fun refreshPaymentsByPropertyUnit(propertyUnitId: Int)
    suspend fun refreshPaymentsByTenant(tenantId: Int)
}