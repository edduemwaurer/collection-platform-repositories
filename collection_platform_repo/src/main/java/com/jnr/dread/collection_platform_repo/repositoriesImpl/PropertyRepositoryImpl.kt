package com.jnr.dread.collection_platform_repo.repositoriesImpl

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jnr.dread.collection_platform_repo.repositories.PropertyRepository
import com.jnr.dread.commons.domain.DomainProperty
import com.jnr.dread.commons.domain.DomainPropertyUnit

class PropertyRepositoryImpl : PropertyRepository {

    private val _properties = MutableLiveData<List<DomainProperty>>()

    private val _propertyUnits = MutableLiveData<List<DomainPropertyUnit>>()

    override val properties: LiveData<List<DomainProperty>>
        get() = _properties

    override val propertyUnits: LiveData<List<DomainPropertyUnit>>
        get() = _propertyUnits

    override fun createProperty() {
        TODO("Not yet implemented")
    }

    override fun createPropertyUnit(propertyId: Long) {
        TODO("Not yet implemented")
    }

    override fun getAllProperties() {
        TODO("Not yet implemented")
    }

    override fun getAllPropertyUnits(propertyId: Long) {
        TODO("Not yet implemented")
    }

    override fun getPropertyById(propertyId: Long) {
        TODO("Not yet implemented")
    }

    override fun updateProperty(propertyId: Long) {
        TODO("Not yet implemented")
    }

    override fun updatePropertyUnit(propertyUnitId: Long) {
        TODO("Not yet implemented")
    }

    override fun deleteProperty(propertyId: Long) {
        TODO("Not yet implemented")
    }

    override fun deletePropertyUnit(propertyUnitId: Long) {
        TODO("Not yet implemented")
    }
}