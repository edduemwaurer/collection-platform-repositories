package com.jnr.dread.collection_platform_repo.repositories

import androidx.lifecycle.LiveData
import com.jnr.dread.commons.domain.DomainProperty
import com.jnr.dread.commons.domain.DomainPropertyUnit

interface PropertyRepository {
    val properties: LiveData<List<DomainProperty>>
    val propertyUnits: LiveData<List<DomainPropertyUnit>>
    fun createProperty()
    fun createPropertyUnit(propertyId: Long)
    fun getAllProperties()
    fun getAllPropertyUnits(propertyId: Long)
    fun getPropertyById(propertyId: Long)
    fun updateProperty(propertyId: Long)
    fun updatePropertyUnit(propertyUnitId: Long)
    fun deleteProperty(propertyId: Long)
    fun deletePropertyUnit(propertyUnitId: Long)
}