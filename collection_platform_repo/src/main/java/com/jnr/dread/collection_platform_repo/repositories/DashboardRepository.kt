package com.jnr.dread.collection_platform_repo.repositories

import androidx.lifecycle.LiveData
import com.jnr.dread.commons.domain.DomainAppFeatures

interface DashboardRepository {
    val appFeatures: LiveData<List<DomainAppFeatures>>
    suspend fun refreshAllAppFeatures()
    suspend fun refreshTenantAppFeatures()
    suspend fun refreshPropertyManagerAppFeatures()
}