package com.jnr.dread.collection_platform_repo.repositories

import androidx.lifecycle.LiveData

interface TenantRepository {
    val allTenants: LiveData<List<Any>>
    suspend fun refreshTenantsByProperty(propertyId: Int)
    suspend fun refreshTenantsByPropertyManager(merchantId: Int)
    fun getTenantById(tenantId: Int)
    suspend fun deleteTenant(tenantId: Int)
    suspend fun updateTenant(tenantId: Int)
    suspend fun createTenants(tenantId: Int)
}