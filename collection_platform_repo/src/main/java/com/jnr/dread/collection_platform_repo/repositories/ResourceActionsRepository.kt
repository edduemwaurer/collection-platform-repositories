package com.jnr.dread.collection_platform_repo.repositories

import androidx.lifecycle.LiveData
import com.jnr.dread.commons.domain.DomainResourceActions

interface ResourceActionsRepository {
    val allResourceActions: LiveData<List<DomainResourceActions>>
    suspend fun refreshAppResourceActions(resourceId: Long, operatorToken: String)
}