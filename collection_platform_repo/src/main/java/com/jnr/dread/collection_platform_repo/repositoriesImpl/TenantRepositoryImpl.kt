package com.jnr.dread.collection_platform_repo.repositoriesImpl

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jnr.dread.collection_platform_repo.repositories.TenantRepository

class TenantRepositoryImpl : TenantRepository {

    private val _tenants = MutableLiveData<List<Any>>()

    override val allTenants: LiveData<List<Any>>
        get() = _tenants

    override suspend fun refreshTenantsByProperty(propertyId: Int) {
        TODO("Not yet implemented")
    }

    override suspend fun refreshTenantsByPropertyManager(merchantId: Int) {
        TODO("Not yet implemented")
    }

    override fun getTenantById(tenantId: Int) {
        TODO("Not yet implemented")
    }

    override suspend fun deleteTenant(tenantId: Int) {
        TODO("Not yet implemented")
    }

    override suspend fun updateTenant(tenantId: Int) {
        TODO("Not yet implemented")
    }

    override suspend fun createTenants(tenantId: Int) {
        TODO("Not yet implemented")
    }
}