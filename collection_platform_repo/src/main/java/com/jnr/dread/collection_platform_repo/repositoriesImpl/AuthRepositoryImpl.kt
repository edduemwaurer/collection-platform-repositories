package com.jnr.dread.collection_platform_repo.repositoriesImpl

import android.util.Log.i
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jnr.dread.collection_platform_repo.repositories.AuthRepository
import com.jnr.dread.commons.domain.DomainOperator

class AuthRepositoryImpl : AuthRepository {

    override suspend fun login(user: String, secret: String): LiveData<DomainOperator> {
        i("AuthRepositoryImpl", "Submitted Operator $user $secret")

        return MutableLiveData()
    }

    override suspend fun register() {
        TODO("Not yet implemented")
    }

    override suspend fun resetPassword(operatorId: Int) {
        TODO("Not yet implemented")
    }

}